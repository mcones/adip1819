//
// Created by cones on 24.10.2018.
//

#ifndef ADIP_MAIN_H
#define ADIP_MAIN_H

int factorial(int n);
int binomial(int n, int k);
int lottery(int n, int k);
void trig();
int succ(int x);
int pre(int x);
int add(int x, int y);
int sub(int x, int y);
int mult(int x, int y);
void findPyt(int limit);
void getChange();

#endif  // ADIP_MAIN_H
