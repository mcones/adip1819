#include <stdio.h>

int main() {
  int n1, n2;
  printf("Geben Sie hier 2 Zahlen ein:");
  scanf("%d", &n1);
  scanf("%d", &n2);
  for (int i = 1; i <= n1; i++) {
    for (int j = 1; j <= n2; j++) {
      printf("%d X %d = %d\n", i, j, i * j);
    }
  }
}
