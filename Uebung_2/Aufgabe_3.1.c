#include <math.h>
#include <stdio.h>

int main() {
  int num;
  printf("Geben Sie hier eine Zahl ein:");
  scanf("%d", &num);
  int sum = 1;
  for (int i = 2; i <= sqrt(num); i++) {
    if (num % i == 0) {
      if (i != sqrt(num)) {
        sum += i + num / i;
      } else {
        sum += i;
      }
    }
  }
  if (sum == num)
    printf("Vollkommen");
  else
    printf("Defizient");
}
