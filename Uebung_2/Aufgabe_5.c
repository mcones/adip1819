#include <stdio.h>

// Die lineare Lösung läuft die Schleife n mal ab und braucht daher 17 Schritte
// für a^17...
double Aufgabe_5a(double a, int n) {
  double result = 1;
  for (int i = n; i > 0; i--) {
    result *= a;
  }
  return result;
}

// ...die elegantere Lösung hingegen nur 5 (dec, div, div, div, div).
double Aufgabe_5b(double a, int n) {
  double result = 1;
  if (n == 0) {
    return 1;
  }
  while (n > 1) {
    if (n % 2 == 0) {
      a *= a;
      n /= 2;
    } else {
      n--;
      result *= a;
    }
  }
  return result * a;
}

int main() {
  printf("A5a: %f\n", Aufgabe_5b(5.2, 3));
  printf("A5b: %f\n", Aufgabe_5a(5.2, 3));
  return 0;
}