#include <stdio.h>

long fib_rec(int n) {
  if (n < 2) {
    return n;
  } else {
    return fib_rec(n - 2) + fib_rec(n - 1);
  }
}

// Warning: Will overflow after 30 even numbers.
// Fib_150 won't fit even in unsigned long long int.
void return_first_even(int n) {
  long f0 = 0;
  long f1 = 1;
  if (n < 1)
    return;
  for (int i = 1; i <= n * 3; i++) {
    if (i % 3 == 0)
      printf("Fib #%i: %li\n", i / 3, f1);
    long f2 = f0 + f1;
    f0 = f1;
    f1 = f2;
  }
}

int main() {

  printf("A2: %li\n", fib_rec(5));
  printf("A2: %li\n", fib_rec(20));
  return_first_even(50);
  return 0;
}