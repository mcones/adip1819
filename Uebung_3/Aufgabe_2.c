#include <stdio.h>
#include <stdlib.h>

double arithMean(double *array, int n) {
  double result = 0;
  for (int i = 0; i < n; i++) {
    result += array[i];
  }
  return result / n;
}

double samVar(double *array, int n) {
  double result = 0;
  double mean = arithMean(array, n);
  for (int i = 0; i < n; i++) {
    result += (mean - array[i]) * (mean - array[i]);
  }
  return result / (n - 1);
}

int main() {
  int n = 0;
  printf("Wie groß ist die Liste?\n");
  scanf("%i", &n);
  double *array = malloc(n * sizeof(double));
  for (int i = 0; i < n; i++) {
    printf("Bitte nächste Zahl eingeben:\n");
    scanf("%lf", &array[i]);
    printf("%f\n", array[i]);
  }
  printf("Der Durchschnitt ist %f\n", arithMean(array, n));
  printf("Die Stichprobenvarianz ist %f\n", samVar(array, n));
  free(array);
  return 0;
}