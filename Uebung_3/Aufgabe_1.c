#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void initializegenerator() { srand(time(NULL)); }

int karteziehen() { return (rand() % 9) + 2; }

int main() {
  initializegenerator();
  int sumP = 0;
  int sumD = karteziehen();
  int move = 1;
  printf("Dealer hat die Karte %i\n", sumD);
  while (move == 1) {
    sumP += karteziehen();
    if (sumP <= 21) {
      printf("Momentane Summe: %i\nNächster Schritt? 1: Draw, 0: Stay\n", sumP);
      scanf("%i", &move);
    } else {
      printf("Verloren! Spieler hat %i\n", sumP);
      return 0;
    }
  }
  while (sumD <= 16) {
    sumD += karteziehen();
  }
  if (sumD > 21 || sumD < sumP) {
    printf("Gewonnen! Dealer hat %i, Spieler hat %i.\n", sumD, sumP);
  } else if (sumD == sumP) {
    printf("Remis! Dealer und Spieler haben %i.\n", sumD);
  } else {
    printf("Verloren! Dealer hat %i, Spieler hat %i.\n", sumD, sumP);
  }

  return 0;
}