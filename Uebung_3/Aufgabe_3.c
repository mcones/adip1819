#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  char prefix[9][10] = {"ein",   "zwei",   "drei", "vier", "fünf",
                        "sechs", "sieben", "acht", "neun"};
  char suffix[5][10] = {
      "zwanzig", "dreißig", "vierzig", "fünfzig", "sechzig",
  };
  int input;
  char result[30] = "";
  printf("Bitte Zahl von 20-69 eingeben.\n");
  scanf("%i", &input);

  if(input % 10 != 0){
      strcat(result, prefix[(input % 10)-1]);
      strcat(result, "-und-");
  }
  strcat(result, suffix[(input/10)-2]);
  printf("%s\n", result);
  return 0;
}